import pandas as pd
from sklearn import model_selection
from apyori import apriori
import argparse               

def argument():
    
    parser = argparse.ArgumentParser(description='Apriori Rule based Model for Store Recommendation System')
    parser.add_argument('--file', type=str,default='store_data.csv' ,help='Dataset Filename (.csv)')
    parser.add_argument('--ci', type=float, default=0.2 ,help='Minimum Confidence Interval')
    parser.add_argument('--min_length', type=int, default=2 ,help='Minimum Product Suggestion Length')

    args = parser.parse_args()
    return args
def data_preprocess(filename):
    data = pd.read_csv(filename)
    # print(data)
    item_list = data.unstack().dropna().unique()
    # print(item_list)
    
    train,test = model_selection.train_test_split(data,test_size=0.10)
    # print('Train:',train)
    # print('Test:',test)
    
    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    # print('Transform')
    # print(train)

    return train,test

def apriori_model(training_data,min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    results = list(apriori(training_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    print('Apriori')
    print(results)        
    print('-------Rule (Apriori) -------')
    for i in results:
        print(i)
        print('--------------------------------------')
    return results
def apriori_visualize(results):
    for rule in results:
        # first index of the inner list
        # Contains base item and add item
        pair = rule[0] 
        items = [x for x in pair]
        print(pair)
        print('Rule: ' + items[0] + ' -> ' + items[1])

        #second index of the inner list
        print('Support: ' + str(rule[1]))

        #third index of the list located at 0th
        #of the third index of the inner list

        print('Confidence: ' + str(rule[2][0][2]))
        print('Lift: ' + str(rule[2][0][3]))
        print('=====================================')

if __name__ == '__main__':
    args = argument()
    print(args.file)
    train, test = data_preprocess(args.file)
    results = apriori_model(train, min_confidence=args.ci,min_length=args.min_length)
    apriori_visualize(results)